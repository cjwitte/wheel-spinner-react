import React from 'react';
import { StyleSheet, Text, View, FlatList, Button, Alert } from 'react-native';

export default class App extends React.Component {
  _onPressSpin() {
    var knownWheelSwaps = ['Left Side', 'Right Side', 'RF & LR', 'LF & RR'];
    var randomSpin = parseInt(Math.random() * knownWheelSwaps.length);
    Alert.alert('' + knownWheelSwaps[randomSpin]);
  }
  render() {
    return (
      <View style={styles.container}>
        <Text />
        <Text>Wheel Spinner App!</Text>
        <Text />
        <Text />
        <Text />
        <Text />
        <Text />
        <Text />
        <Text />
        <Text />
        <Text />
        <Text />
        <Text />
        <Text />
        <Text />
        <Text />
        <Text />
        <Text />
        <Text />
        <Button
          onPress={this._onPressSpin}
          title="Press HERE for Wheel Swap!"
          color="#697849"
          accessibilityLabel="Wheelswap spinnger"
        />
        <FlatList
          data={[{ key: ' ' }]}
          renderItem={({ item }) => (
            <View>

              <Text>{item.key}</Text>

            </View>
          )}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

